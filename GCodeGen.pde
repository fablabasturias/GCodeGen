// Simple drawing and GCODE generator 
// David Pello 2015
// LABoral Centro de Arte y Creación Industrial


// configuration

int NORMALSPEED = 5000;
int JUMPSPEED = 10000;
float EXTRUSIONSPEED = 0.01;
int PRINT_HEIGHT = 1;

boolean GO_UP_WHEN_JUMPING = true;
int GO_UP_DISTANCE = 2;


// Globals

boolean drawing = false;
boolean previousDrawing = false;
int previousX, previousY;

// Data structure for our drawings
class Movement {
  int x;
  int y;
  boolean penDown;
  float len;

  Movement (int nx, int ny, boolean pd, float l) {
    x = nx;
    y = ny;
    penDown = pd;
    len = l;
  } 

}  

// Arraylist of all the movements
ArrayList<Movement> movements = new ArrayList<Movement>();


// setup the screen
void setup(){
  size(400,400);
  background(0);
  stroke(100,100,255);
  smooth();
  noFill();

  // draw the petri dish
  ellipse (width/2, height/2, 185*2, 185*2);
  stroke(255);

  // init variables
  previousX=width/2;
  previousY=height/2;

}


void draw() {

  // check for keyboard events
  if (drawing) {
    // clear previous text
    fill (0);
    stroke(0);
    rect(5, height-15-12, 100, 20);

    // draw text
    fill(255,0,0);
    stroke(255,0,0);
    textSize(12);
    text ("Drawing", 5, height-15);
    fill(255);
    stroke(255);
  }
  else {
    // clear previous text
    fill (0);
    stroke(0);
    rect(5, height-15-12, 100, 20);

    // draw text
    fill(255);
    stroke(255);
    textSize(12);
    text ("Not Drawing", 5, height-15);
  }

}

void keyPressed() {
  // draw
  if (key == 'd' || key == 'D') {
    drawing = !drawing;
  }
  // generate G-code
  if (key == 'g' || key == 'G') {
    generateGCode();
  }
  // clean
  if (key == 'c' || key == 'C') {
    movements.clear();
    // clear screen
    drawing = false;
    fill (0);
    stroke(0);
    ellipse(width/2, height/2, 180*2, 180*2);
    stroke(255);
  }
  // rotate
  if (key >='2' && key <='8') {
    int number = key - 48;
    rotateDrawing(number);
  }
}

void mouseClicked() {
  if (drawing) {
    // check for valid coordinates (inside the dish)
    if (pow((mouseX - (width/2)),2) + pow((mouseY - (height/2)),2) <= pow(180,2)) {
      // ok, draw
      line(previousX, previousY, mouseX, mouseY);
      
      // if previous coordinate wasn't drawing store it
      if (!previousDrawing)
        movements.add(new Movement(previousX, previousY, previousDrawing, 0));
      // and now calculate lenght and store the current one
      float l = sqrt(pow(abs(mouseX-previousX),2)+pow(abs(mouseY-previousY),2));
      movements.add(new Movement(mouseX, mouseY, drawing, l));

      // remember previous values
      previousX = mouseX;
      previousY = mouseY;
      previousDrawing = true;
    }
  }
  else {
    if (pow((mouseX - (width/2)), 2) + pow((mouseY - (height/2)),2) <= pow(180,2)) {
      // store position
      previousX = mouseX;
      previousY = mouseY;
      previousDrawing = false;
    }
  }

}


void rotateDrawing(int times) {
  // calculate the angle of each rotation
  float angle = (360 / times)*PI/180;
 
  // create a temporary array
  ArrayList<Movement> tempMovements = new ArrayList<Movement>(); 

  // rotate n times
  for (int i=0; i<times-1; i++) {
    // for each point
    for (int j=0; j<movements.size(); j++) {
      Movement m = movements.get(j);

      // calculate new coordinates
      float x = (cos(angle*(i+1)) * (m.x - (width/2))) - (sin(angle*(i+1)) * (m.y - (height/2))) + width/2;
      float y = (sin(angle*(i+1)) * (m.x - (width/2))) + (cos(angle*(i+1)) * (m.y - (height/2))) + height/2;
      
      // add new point to temp array
      tempMovements.add(new Movement(round(x), round(y), m.penDown, m.len));
    }
  }

  // ok, now draw the new points
  for (int i=0; i<tempMovements.size()-1; i++) {
    Movement m1 = tempMovements.get(i);
    Movement m2 = tempMovements.get(i+1);
    if (m2.penDown)
      line(m1.x, m1.y, m2.x, m2.y);
  }

  // and add the new points to the old ones
  movements.addAll(tempMovements);
}

void generateGCode(){
  String header = 
    "G90\n" +                             // absolute positioning
    "G21\n" +                             // millimeters
    "G1 Z40 F5000\n" +                    // move Z up 
    "G28\n" +                             // home
    "G1 Z40 F5000\n" +                    // move Z up again
    "G1 X100 Y100 F5000\n" +              // move to center
    "G1 Z" + PRINT_HEIGHT + " F5000\n" +  // move Z to print height
    "M42 P4 S255\n";                      // turn on pin P4
  
  String footer = 
    "M42 P4 S0\n" +                       // turn off pin P4
    "G1 Z40 F5000\n" +                    // move Z up
    "G28 X0 Y0\n" +                       // home X and Y
    "M84\n" +                             // stop motors
    "M104 S0\n";                          // turn off heat

  String gcode = "";

  gcode += header;

  for (int i=0; i<movements.size(); i++) {
    Movement m = movements.get(i);
    if (m.penDown) {
      gcode += "G1 F" + NORMALSPEED + " X" + m.x/2 + " Y" + m.y/2;
      gcode += " E" + EXTRUSIONSPEED*m.len + "\n";
    }
    else {
      if (GO_UP_WHEN_JUMPING)
        gcode += "G1 Z" + GO_UP_DISTANCE + " F5000 \n";
      
      gcode += "G1 F" + JUMPSPEED + " X" + m.x/2 + " Y" + m.y/2 + "\n";
      
      if (GO_UP_WHEN_JUMPING)
        gcode += "G1 Z" + PRINT_HEIGHT + " F5000 \n";
    }
  }

  gcode += footer;

  print(gcode);
  
  // save to file
  PrintWriter output;
  output = createWriter("output.gcode");
  output.print(gcode);
  output.flush();
  output.close();


}
